/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package answer;

import java.io.File;

/**
 *
 * @author omega
 */  

public class Answer {
    int QuestionID;
    int StenerID;
    String Response;
    File Evidence;
    String Evidence_Name = "";

    public Answer(int QuestionID, int StenerID, String Response, File Evidence, String Evidence_Name) {
        this.QuestionID=QuestionID;
        this.StenerID=StenerID;
        this.Response=Response;
        this.Evidence=Evidence;
        this.Evidence_Name = Evidence_Name;
    }
    
    public Answer(int QuestionID, int StenerID, String Response, File Evidence) {
        this.QuestionID=QuestionID;
        this.StenerID=StenerID;
        this.Response=Response;
        this.Evidence=Evidence;
    }
    
    public Answer(int QuestionID, int StenerID, String Response) {
        this.QuestionID = QuestionID;
        this.StenerID = StenerID;
        this.Response=Response;
    }
    
    public Answer(int QuestionID, int StenerID) {
        this.QuestionID = QuestionID;
        this.StenerID = StenerID;
    }
    
    public int getQuestionID() {
        return QuestionID;
    }

    public void setQuestionID(int QuestionID) {
        this.QuestionID = QuestionID;
    }

    public int getStenerID() {
        return StenerID;
    }

    public void setStenerID(int StenerID) {
        this.StenerID = StenerID;
    }

    public String getResponse() {
        return Response;
    }

    public void setResponse(String Response) {
        this.Response = Response;
    }

    public File getEvidence() {
        return Evidence;
    }

    public void setEvidence(File Evidence) {
        this.Evidence = Evidence;
    }
    
    public String getEvidence_Name()
    {
        return Evidence_Name;
    }
    
    public void setEvidence_Name(String Evidence_Name) {
        this.Evidence_Name = Evidence_Name;
    }


  
    
    
    
    
    
}

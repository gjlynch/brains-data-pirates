/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package answer;

import stener.Stener;
import brains.CloudSQL;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.nio.file.Files;

/**
 *
 * @author omega
 */
public class AnswerActions {
     public static boolean addAnswer(Answer answer) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "INSERT INTO Answers (QuestionID ,StenerID)" +
                        "VALUES ( '"+answer.getQuestionID()+"', '"+answer.getStenerID()+"' )");
                  
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
     
    public static boolean addCompleteAnswer(Answer answer) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        byte[] fileContent = new byte[(int) answer.getEvidence().length()];
        FileInputStream is = new FileInputStream(answer.getEvidence());
        is.read(fileContent);  
        
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE Answers SET " +
                            "Response='"+answer.getResponse()+
                            "', Evidence='"+ new String(fileContent)+
                            "', Evidence_Name='"+answer.getEvidence_Name()+
                            "' WHERE QuestionID="+answer.getQuestionID()
                            +" AND StenerID="+answer.getStenerID());
                  
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
    
    public static int addNewAnswer(Answer answer) throws IOException, SQLException {
        int added = 0;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "INSERT INTO Answers (QuestionID ,StenerID)"
                    + "VALUES ( '" + answer.getQuestionID() + "', '" + answer.getStenerID() + "' )");

            added += 1;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return added;
    }
    
    public static void deleteAnswer(Answer answer) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "DELETE FROM Answers WHERE "
                    + "QuestionID='" + answer.getQuestionID() + "' AND StenerID='" + answer.getStenerID() + "'");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    
}

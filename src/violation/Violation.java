/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package violation;

/**
 *
 * @author glync
 */
public class Violation {
    private String time, date, description, rating, status;
    private int department, questionID, stenerID, violationID;

    public Violation(String time, String date, String description, String rating, int department, int questionID, int stenerID, int violationID, String status) {
        this.time = time;
        this.date = date;
        this.description = description;
        this.rating = rating;
        this.department = department;
        this.questionID = questionID;
        this.stenerID = stenerID;
        this.violationID = violationID;
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public int getStenerID() {
        return stenerID;
    }

    public void setStenerID(int stenerID) {
        this.stenerID = stenerID;
    }

    public int getViolationID() {
        return violationID;
    }

    public void setViolationID(int violationID) {
        this.violationID = violationID;
    }
    

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public int getDepartment() {
        return department;
    }

    public void setDepartment(int department) {
        this.department = department;
    }
    
    
    
}

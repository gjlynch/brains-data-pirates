/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package violation;

import brains.CloudSQL;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import violation.Violation;

/**
 *
 * @author glync
 */
public class ViolationActions {
    
    public static void addViolation(Violation violation) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "INSERT INTO Violation (ViolationID, QuestionID, StenerID, Origin, Description, Rating, Status, Time, Date)"
                    + "VALUES ('" + violation.getViolationID() 
                            + "', '" + violation.getQuestionID()
                            + "', '" + violation.getStenerID()
                            + "', '" + violation.getDepartment()
                            + "', '" + violation.getDescription()
                            + "', '" + violation.getRating()
                            + "', '" + violation.getStatus()
                            + "', '" + violation.getTime()
                            + "', '" + violation.getDate()+ "')");      
                statement.executeUpdate("UPDATE Stener SET Can_Edit='True', Resubmission_Status='True' WHERE StenerID='"+violation.getStenerID()+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void deleteViolation(int violation) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM Violation WHERE ViolationID='"+violation+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void editViolation(Violation violation) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("UPDATE Violation SET " +
                    "Description='"+violation.getDescription()
                    +"', Rating='"+ violation.getRating()
                    +"', Status='"+ violation.getStatus()
                    +"', Time='"+ violation.getTime()
                    +"', Date='"+ violation.getDate()
                    +"' WHERE ViolationID='"+violation.getViolationID()+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}

package question;

public class Question {
    
    private int questionID;
    private String content, help;
    
    public Question(){ }
    
    public Question(int questionID, String content, String help){
        this.questionID=questionID;
        this.content=content;
        this.help=help;
    }
    
    public Question(String content, String help){
        this.questionID=questionID;
        this.content=content;
        this.help=help;
    }

    public int getQuestionID() {
        return questionID;
    }

    public void setQuestionID(int questionID) {
        this.questionID = questionID;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }
    
    
}

package question;

import brains.CloudSQL;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javafx.event.ActionEvent;
import user.User;


public class QuestionActions {
    
    public static boolean addQuestion(Question question) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                     "INSERT INTO Question (QuestionID, Help, Content)" +
                        "VALUES ('"+question.getQuestionID()+"', '"+question.getHelp()
                             +"', '"+question.getContent()+"')");
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
    
    public static boolean editQuestion(Question question, int originalID) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                     "UPDATE Question SET QuestionID="+Integer.toString(question.getQuestionID())+", Help='"+question.getHelp()
                             +"', Content='"+question.getContent()
                             +"' WHERE QuestionID="+Integer.toString(originalID));
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
    
    public static void deleteQuestion(int questionID) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM Answers WHERE QuestionID='" +questionID+"'" );
            statement.executeUpdate("DELETE FROM Violation WHERE QuestionID='" + questionID + "'");
            statement.executeUpdate("DELETE FROM Question WHERE QuestionID='" + questionID+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    
    
    
}

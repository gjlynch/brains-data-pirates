/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;

import brains.CloudSQL;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class UserActions {
    
    public UserActions() { }
   
    public static boolean validate(User user) throws IOException, SQLException{
        boolean validate = false;
        //get database connection
        Connection connection = CloudSQL.getConnection();
        //create statement and enter query
        try (Statement statement = connection.createStatement()) {
            //results stored in resultSet
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM User WHERE Username = '" 
                        + user.getUsername().toLowerCase()
                        + "' and Pass = '" 
                        + user.getPassword()+"'");
            //check results to see if username and password enter match 
            //set validate to true if they do
            while(resultSet.next()){
                if(resultSet.getString("Username").equals(user.getUsername()) 
                        && resultSet.getString("Pass").equals(user.getPassword())){
                    validate=true;
                }
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return validate;
    }
    
    public static String Type(User user) throws IOException, SQLException{
        String type=null;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM User WHERE Username = '" 
                        + user.getUsername()
                        + "' and Pass = '" 
                        + user.getPassword()+"'");
            while(resultSet.next()){
                type=resultSet.getString("Account_Type");
               
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return type;
    }
    
    public static boolean addUser(User user) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                     "INSERT INTO User (Username, Pass, Department, Account_Type, Employee_Name)" +
                        "VALUES ('"+user.getUsername().toLowerCase()+"', '"+user.getPassword()
                             +"', '"+user.getDepartment()+"', '"
                                    +user.getType()+"', '"+user.getName()+"')");
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
    
    public static Integer getDepNumber(User user) throws IOException, SQLException
    {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM User WHERE Username = '" 
                        + user.getUsername()
                        + "' and Pass = '" 
                        + user.getPassword()+"'");
            while(resultSet.next()){
                return resultSet.getInt("Department");
            }
            
        } catch (Exception e) {
            e.printStackTrace();
        }
        return -1;
    }
    
    public static void editUser(User user, String username) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE User SET Username='"+user.getUsername().toLowerCase()
                            +"', Pass='"+user.getPassword()
                            +"', Department='"+user.getDepartment()
                            +"', Account_Type='"+user.getType()
                            +"', Employee_Name='"+user.getName()
                            +"' WHERE Username='"+username+"'");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public static void editUserNoPass(User user, String username) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE User SET Username='"+user.getUsername().toLowerCase()
                            +"', Department='"+user.getDepartment()
                            +"', Account_Type='"+user.getType()
                            +"', Employee_Name='"+user.getName()
                            +"' WHERE Username='"+username+"'");

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
    public static void deleteUser(String username) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("DELETE FROM User WHERE Username='"+username+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
    
}

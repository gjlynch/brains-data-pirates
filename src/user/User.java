/*
/*
 * To change this license header, choose License Headers in Project Properties.
 * To chthis template file, choose Tools | Templates
 * and open the template in the editor.
 */
package user;


/**
 *
 * @author glync
 */
public class User {
    private String username, password, name, type;
    private int department;

    public User() { }
    
    public User(String username, String password) {
        this.username=username;
        this.password=password;
    }
    
    public User(String username, String password, String name, String type, int department) {
        this.username=username;
        this.password=password;
        this.department=department;
        this.name=name;
        this.type=type;
    }
    
    public User(String username, String name, String type, int department) {
        this.username=username;
        this.department=department;
        this.name=name;
        this.type=type;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setDepartment(int department) {
        this.department = department;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    public int getDepartment() {
        return department;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    
    public void setUsername(String username) {
        this.username = username;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
}

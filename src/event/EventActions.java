/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package event;

import brains.CloudSQL;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDate;

/**
 *
 * @author omega
 */
public class EventActions {
     public static void addEvent(Event event) throws IOException, SQLException{
         LocalDate a = LocalDate.now();
         String DateLong = a.toString();
         String Date = DateLong;
         int max=0;
         event.setDate(Date);
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
              ResultSet r= statement.executeQuery("SELECT MAX(EventID) AS ID FROM Event");
            while(r.next()){
                max= r.getInt("ID");
            }     
            max=max+1;
            event.setEventID(max);
            statement.executeUpdate(
                     "INSERT INTO Event (EventID, Date, Action, Username)" +
                        "VALUES ('"+event.getEventID()+"', '"+event.getDate()
                             +"', '"+ event.getAction()+"','"+ event.getUsername()+"')");
          
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        
    }
}

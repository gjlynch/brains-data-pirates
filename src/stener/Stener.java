/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stener;

/**
 *
 * @author omega
 */public class Stener {
    
    private int StenerID;
    private int NumQuestions;
    private String dueDate;
    private String canEdit;
    private String resubStatus;
    private int DepNumber;
    private int SetID;
    private String accepted;
    
    public Stener(){ }

  

    public int getStenerID() {
        return StenerID;
    }

    public void setStenerID(int StenerID) {
        this.StenerID = StenerID;
    }

    public int getNumQuestions() {
        return NumQuestions;
    }

    public void setNumQuestions(int NumQuestions) {
        this.NumQuestions = NumQuestions;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public String getCanEdit() {
        return canEdit;
    }

    public void setCanEdit(String canEdit) {
        this.canEdit = canEdit;
    }

    public String getResubStatus() {
        return resubStatus;
    }

    public void setResubStatus(String resubStatus) {
        this.resubStatus = resubStatus;
    }

    public int getDepNumber() {
        return DepNumber;
    }

    public void setDepNumber(int DepNumber) {
        this.DepNumber = DepNumber;
    }

    public int getSetID() {
        return SetID;
    }

    public void setSetID(int SetID) {
        this.SetID = SetID;
    }

    

    public Stener(int StenerID, int NumQuestions, String dueDate, String canEdit, String resubStatus, int SetID) {
        this.StenerID = StenerID;
        this.NumQuestions = NumQuestions;
        this.dueDate = dueDate;
        this.canEdit = canEdit;
        this.resubStatus = resubStatus;
        this.SetID = SetID;
    }

 
    public Stener(int StenerID, int NumQuestions, String dueDate, String canEdit, String resubStatus, int DepNumber, int SetID) {
        this.StenerID = StenerID;
        this.NumQuestions = NumQuestions;
        this.dueDate = dueDate;
        this.canEdit = canEdit;
        this.resubStatus = resubStatus;
        this.DepNumber = DepNumber;
        this.SetID = SetID;
    }
    
    public Stener(int StenerID, int NumQuestions, String dueDate, String canEdit, String resubStatus, int DepNumber, int SetID, String Accepted) {
        this.StenerID = StenerID;
        this.NumQuestions = NumQuestions;
        this.dueDate = dueDate;
        this.canEdit = canEdit;
        this.resubStatus = resubStatus;
        this.DepNumber = DepNumber;
        this.SetID = SetID;
        this.accepted=Accepted;
    }

    public String getAccepted() {
        return accepted;
    }

    public void setAccepted(String accepted) {
        this.accepted = accepted;
    }
    

 }



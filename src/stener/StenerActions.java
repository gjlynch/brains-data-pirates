/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package stener;

import brains.CloudSQL;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import stener.Stener;
/**
 *
 * @author omega
 */
public class StenerActions {
     public static boolean addTemplateStener(Stener stener) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("INSERT INTO Stener (StenerID, Questions, Due_date, Can_Edit, Resubmission_Status, SetID) " +
                            "VALUES ('"+stener.getStenerID()
                             +"', '"+stener.getNumQuestions()
                             +"', '"+stener.getDueDate()
                             +"', '"+stener.getCanEdit()
                             +"', '"+stener.getResubStatus()
                             +"', '"+stener.getSetID()+"')");
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
     
    public static boolean assignStener(Stener stener) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate("INSERT INTO Stener (StenerID, Questions, Due_date, Can_Edit, Resubmission_Status, DepNumber, SetID) " +
                            "VALUES ('"+stener.getStenerID()
                             +"', '"+stener.getNumQuestions()
                             +"', '"+stener.getDueDate()
                             +"', '"+stener.getCanEdit()
                             +"', '"+stener.getResubStatus()
                             +"', '"+stener.getDepNumber()
                             +"', '"+stener.getSetID()+"')");
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
    
    public static int getDepartmentNumber(int id) throws IOException, SQLException {
        int dep = 0;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet r = statement.executeQuery(
                    "SELECT * FROM Stener WHERE StenerID='" + id + "'");
            while (r.next()) {
                dep = r.getInt("DepNumber");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return dep;
    }
    
    public static void deleteStener(int setID) throws IOException, SQLException {
        try (Connection connection = CloudSQL.getConnection();
                Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "DELETE FROM Answers WHERE StenerID IN ( SELECT StenerID from Stener where SetID ='" + setID + "')");
            statement.executeUpdate("DELETE FROM Stener where SetID ='" + setID + "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    
    
    
    
}

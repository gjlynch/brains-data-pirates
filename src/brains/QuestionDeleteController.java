/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import question.QuestionActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class QuestionDeleteController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static int questionID;
    @FXML
    private Button cancelQD;
    @FXML
    private Button confirmQD;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cancelQuestionDelete(ActionEvent event) {
                    
        QuestionMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("QuestionMaintenance.fxml", event);
    }

    @FXML
    private void confirmQuestionDelete(ActionEvent event) throws IOException, SQLException {      
        QuestionActions.deleteQuestion(questionID);
        EventActions.addEvent(new Event(" Deleted Question "+questionID ,usernameEvent));

        pageChanger.swap("QuestionMaintenance.fxml", event);
    }
    
}

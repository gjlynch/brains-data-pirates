/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import user.UserActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class UserDeleteController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static String username;
    @FXML
    private Button confirmUD;
    @FXML
    private Button cancelUD;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void confirmUserDelete(ActionEvent event) throws IOException, SQLException {
        UserActions.deleteUser(username);
                UserMaintenanceController.usernameEvent = usernameEvent;
            EventActions.addEvent(new Event(" Deleted User " +username ,usernameEvent));

        pageChanger.swap("UserMaintenance.fxml", event);
    }

    @FXML
    private void cancelUserDelete(ActionEvent event) {
                UserMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("UserMaintenance.fxml", event);
    }
    
}

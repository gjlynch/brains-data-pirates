/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import static brains.StenerListController.usernameEvent;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class OversightScreenController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private Button userMaintenance;
    @FXML
    private Button questionMaintenance;
    @FXML
    private Button stenerMaintenance;
    @FXML
    private Button stenerReview;
    @FXML
    private Button violationMaintenance;
    @FXML
    private Button eventLog;
    @FXML
    private Button logoutButton;
    @FXML
    private Button departmentMaintenance;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void menuItemSelected(ActionEvent event) {
        String page=null;
        
        if(event.getSource().equals(userMaintenance)){
            UserMaintenanceController.usernameEvent = usernameEvent;
            page="UserMaintenance.fxml";
        }
        else if(event.getSource().equals(questionMaintenance)){
            QuestionMaintenanceController.usernameEvent = usernameEvent;
            page="QuestionMaintenance.fxml";
        }
        else if(event.getSource().equals(stenerMaintenance)){
            StenerMaintenanceController.usernameEvent = usernameEvent;
            page="StenerMaintenance.fxml";
        }
        else if(event.getSource().equals(stenerReview)){
            StenerReviewListController.usernameEvent = usernameEvent;
            page="StenerReviewList.fxml";
        }
        else if(event.getSource().equals(violationMaintenance)){
            ViolationMaintenanceController.usernameEvent = usernameEvent;
            page="ViolationMaintenance.fxml";
        }
        else if(event.getSource().equals(eventLog)){
                        EventLogController.usernameEvent = usernameEvent;

            page="EventLog.fxml";
        }
        else if(event.getSource().equals(logoutButton)){
            page="Login.fxml";
        }
        else if(event.getSource().equals(departmentMaintenance)){
            DepartmentMaintenanceController.usernameEvent = usernameEvent;

            page="DepartmentMaintenance.fxml";
        }
        pageChanger.swap(page, event);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import violation.Violation;
import violation.ViolationActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class UpdateViolationController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static int depNumber, QuestionID, StenerID;
    static Violation v;
    static boolean editViolation=false;
    @FXML
    private Button cancelButton;
    @FXML
    private Button submitButton;
    @FXML
    private TextField timeField;
    @FXML
    private TextField dateField;
    @FXML
    private TextArea description;
    @FXML
    private TextField rating;
    @FXML
    private Label stenerLabel;
    @FXML
    private Label departmentLabel;
    @FXML
    private Label questionLabel;
    @FXML
    private Label errorLabel;
    @FXML
    private TextField statusField;
    @FXML
    private Label statusLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        errorLabel.setText("");
        stenerLabel.setText("Stener: "+StenerID);
        questionLabel.setText("Question: "+QuestionID);
        departmentLabel.setText("Department: "+depNumber);
        statusLabel.setVisible(false);
        statusField.setVisible(false);
        
        if(editViolation){
            loadData();
        }
    } 
    
    private void loadData(){
        stenerLabel.setText("Stener: " + v.getStenerID());
        questionLabel.setText("Question: " + v.getQuestionID());
        departmentLabel.setText("Department: " + v.getDepartment());
        timeField.setText(v.getTime());
        dateField.setText(v.getDate());
        description.setText(v.getDescription());
        rating.setText(v.getRating());
        statusLabel.setVisible(true);
        statusField.setVisible(true);
        statusField.setText(v.getStatus());
    }

    @FXML
    private void cancelActions(ActionEvent event) {
        if(editViolation){
            pageChanger.swap("ViolationMaintenance.fxml", event);
        }
        else{
            StenerReviewFormController.stenerID = StenerID;
            pageChanger.swap("StenerReviewForm.fxml", event);
        }
    }

    @FXML
    private void submitViolation(ActionEvent event) throws IOException, SQLException {
        if(timeField.getText().isEmpty() || dateField.getText().isEmpty() || description.getText().isEmpty() || rating.getText().isEmpty()){
            errorLabel.setText("All fields must be filled out before submission");
        }
        else{
            if (editViolation) {
                Violation v2 = new Violation(timeField.getText(), dateField.getText(), description.getText(), 
                        rating.getText(), v.getDepartment(), v.getQuestionID(), v.getStenerID(), v.getViolationID(), statusField.getText());
                ViolationActions.editViolation(v2);
                        ViolationMaintenanceController.usernameEvent = usernameEvent;
                    EventActions.addEvent(new Event(" Violation "+v.getViolationID()+ "was edited" ,usernameEvent));
     
                pageChanger.swap("ViolationMaintenance.fxml", event);
            } else { 
                int vioID = 0;
                Connection connection = CloudSQL.getConnection();
                //get max violation id if there is one
                try (Statement statement = connection.createStatement()) {
                    ResultSet r = statement.executeQuery("SELECT MAX(ViolationID) AS V FROM Violation");
                    while (r.next()) {
                        vioID = r.getInt("V");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Violation v2 = new Violation(timeField.getText(), dateField.getText(), description.getText(), rating.getText(), depNumber, QuestionID, StenerID, vioID += 1, "Open");
                ViolationActions.addViolation(v2);
                StenerReviewFormController.stenerID = StenerID;
                StenerReviewFormController.usernameEvent = usernameEvent;
                EventActions.addEvent(new Event(" Created violation "+vioID ,usernameEvent));

                pageChanger.swap("StenerReviewForm.fxml", event);
            }              
        }
        
    }
    
}

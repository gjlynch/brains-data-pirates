/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import department.Department;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import question.Question;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class DepartmentMaintenanceController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private Button menu;
    @FXML
    private Label errorMessage;
    @FXML
    private TableView<Department> departmentTable;
    @FXML
    private TableColumn<Department, Integer> depNumC;
    @FXML
    private TableColumn<Department, String> depNamC;
    @FXML
    private TableColumn<Department, String> depLocC;
    @FXML
    private Button addDepartment;
    @FXML
    private Button editDepartment;
    @FXML
    private Button deleteDepartment;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        errorMessage.setText("");

        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadData() throws IOException, SQLException {
        ObservableList departments = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM Department");
            while (resultSet.next()) {
                int id = resultSet.getInt("DepNumber");
                String name = resultSet.getString("Name");
                String location = resultSet.getString("Location");
                departments.add(new Department(id, name, location));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
     
        depNumC.setCellValueFactory(new PropertyValueFactory<>("DepNumber"));
        depNamC.setCellValueFactory(new PropertyValueFactory<>("Name"));
        depLocC.setCellValueFactory(new PropertyValueFactory<>("Location"));
        //populate table from observable list
        departmentTable.setItems(departments);

    }

    @FXML
    private void returnToMenu(ActionEvent event) {
                OversightScreenController.usernameEvent = usernameEvent;

        pageChanger.swap("OversightScreen.fxml", event);
    }

    @FXML
    private void addDepartment(ActionEvent event) {
                        UpdateDepartmentController.usernameEvent = usernameEvent;

        pageChanger.swap("UpdateDepartment.fxml", event);
    }

    @FXML
    private void editDepartment(ActionEvent event) {
        UpdateDepartmentController.usernameEvent = usernameEvent;

        UpdateDepartmentController.edit=true;
        UpdateDepartmentController.department=departmentTable.getSelectionModel().getSelectedItem();
        pageChanger.swap("UpdateDepartment.fxml", event);
    }

    @FXML
    private void deleteDepartment(ActionEvent event) {
        DepartmentDeleteController.usernameEvent = usernameEvent;

        DepartmentDeleteController.depNum=departmentTable.getSelectionModel().getSelectedItem().getDepNumber();
        pageChanger.swap("DepartmentDelete.fxml", event);
    }
    
}

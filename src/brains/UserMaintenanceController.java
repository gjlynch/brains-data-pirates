/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import department.Department;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import user.User;
import user.UserActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class UserMaintenanceController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private Button menu;
    @FXML
    private TableColumn<User, Integer> depNamC;
    @FXML
    private Label errorMessage;
    @FXML
    private TableColumn<User, String> userC;
    @FXML
    private TableColumn<User, String> accC;
    @FXML
    private TableColumn<User, String> empC;
    @FXML
    private Button addUser;
    @FXML
    private Button editUser;
    @FXML
    private Button deleteUser;
    @FXML
    private TableView<User> userTable;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(UserMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UserMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void loadData() throws IOException, SQLException{
        ObservableList users = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM User");
            while (resultSet.next()) {
                int department = resultSet.getInt("Department");
                String name = resultSet.getString("Employee_Name");
                String username = resultSet.getString("Username");
                String type = resultSet.getString("Account_Type");
                users.add(new User(username, name, type, department));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
     
        userC.setCellValueFactory(new PropertyValueFactory<>("Username"));
        depNamC.setCellValueFactory(new PropertyValueFactory<>("Department"));
        empC.setCellValueFactory(new PropertyValueFactory<>("Name"));
        accC.setCellValueFactory(new PropertyValueFactory<>("Type"));
        //populate table from observable list
        userTable.setItems(users);
    }

    @FXML
    private void returnToMenu(ActionEvent event) {
                OversightScreenController.usernameEvent = usernameEvent;

        pageChanger.swap("OversightScreen.fxml", event);     
    }

    @FXML
    private void addUser(ActionEvent event) {
        UpdateUserController.usernameEvent = usernameEvent;

        pageChanger.swap("UpdateUser.fxml", event); 
    }

    @FXML
    private void editUser(ActionEvent event) {
        UpdateUserController.edit=true;
        UpdateUserController.user=userTable.getSelectionModel().getSelectedItem();
        UpdateUserController.usernameEvent = usernameEvent;

        pageChanger.swap("UpdateUser.fxml", event); 
    }

    @FXML
    private void deleteUser(ActionEvent event) {
                UserDeleteController.usernameEvent = usernameEvent;

        UserDeleteController.username=userTable.getSelectionModel().getSelectedItem().getUsername();
        pageChanger.swap("UserDelete.fxml", event); 
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import answer.Answer;
import answer.AnswerActions;
import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import question.Question;
import stener.Stener;
import stener.StenerActions;

/**
 * FXML Controller class
 *
 * @author omega
 */
public class StenerModificationController implements Initializable {

    static int StenerId;
    static int SetId;
    static String DueDate;
    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private TableView<Question> QuestionsAvailableTable;
    @FXML
    private TableColumn<Question, Integer> QuestionsAvailableIdColumn;
    @FXML
    private TableView<Question> StenerQuestionTable;
    @FXML
    private TableColumn<Question, Integer> StenerQuestionIDColumn;
    @FXML
    private Label QuestionsAvailableLabel;
    @FXML
    private Button AddQuest;
    @FXML
    private Button RemoveQuest;
    @FXML
    private Button CancelBtn;
    @FXML
    private TextField dueDateTextfield;
    @FXML
    private Label DueDateLabel;
    @FXML
    private TextArea QuestionContentTextArea;
    @FXML
    private Button submitbtn;
    ObservableList table2 = FXCollections.observableArrayList();
    ObservableList originalQuestions = FXCollections.observableArrayList();
    private ArrayList <Question> newQuestions;
   
    private ArrayList <Question> startQuestions;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
      
        
        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        dueDateTextfield.setText(DueDate);
        QuestionsAvailableTable.getSelectionModel().selectedItemProperty().addListener(new InvalidationListener() {
            @Override
            public void invalidated(Observable newValue) {
                QuestionContentTextArea.clear();
                QuestionContentTextArea.appendText(QuestionsAvailableTable.getSelectionModel().getSelectedItem().getContent());
            }
        });
    }    

    @FXML
    private void AddQuestion(ActionEvent event) {
        ObservableList table2 = StenerQuestionTable.getItems();
        table2.add(QuestionsAvailableTable.getSelectionModel().getSelectedItem());
        QuestionsAvailableTable.getItems().removeAll( QuestionsAvailableTable.getSelectionModel().getSelectedItem());
        StenerQuestionTable.setItems(table2);
    }

    @FXML
    private void RemoveQuestion(ActionEvent event) {
        ObservableList table2 = QuestionsAvailableTable.getItems();
        table2.add(StenerQuestionTable.getSelectionModel().getSelectedItem());
        StenerQuestionTable.getItems().removeAll( StenerQuestionTable.getSelectionModel().getSelectedItem());
        QuestionsAvailableTable.setItems(table2);
    }

    @FXML
    private void CancelAction(ActionEvent event) {
    StenerMaintenanceController.usernameEvent = usernameEvent;

         pageChanger.swap("StenerMaintenance.fxml", event);
    }

    @FXML
    private void Submit(ActionEvent event) throws IOException, SQLException {
        int added=0;
        int numQuestions=StenerQuestionTable.getItems().size();
        String duedate = dueDateTextfield.getText();
        ArrayList<String> stenerIDs=new ArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet r = statement.executeQuery(
                    "SELECT * FROM Stener WHERE SetID='" + SetId + "'");
            while(r.next()){
                stenerIDs.add(r.getString("StenerID"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        for(String sID: stenerIDs){
            //remove questions
            for (Question q : QuestionsAvailableTable.getItems()) {
                int questionID = q.getQuestionID();
                AnswerActions.deleteAnswer(new Answer(questionID, Integer.parseInt(sID)));
            }

            //add new questions 
            for (Question q : StenerQuestionTable.getItems()) {
                int questionID = q.getQuestionID();
                added += AnswerActions.addNewAnswer(new Answer(questionID, Integer.parseInt(sID)));
            }
            //update due date
            try (Statement statement = connection.createStatement()) {
                statement.executeUpdate(
                        "UPDATE Stener SET" + " Due_date='" + duedate + "', Questions='"+numQuestions+"' WHERE StenerID='" + sID + "'");
            } catch (Exception e) {
                e.printStackTrace();
            }
            //set stener can_edit status and complete status if adding question 
            if (added > 0) {
                try (Statement statement = connection.createStatement()) {
                    statement.executeUpdate(
                            "UPDATE Stener SET"
                            + " Can_Edit='True', Accepted='False'" + " WHERE StenerID='" + sID + "'");

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            
        }
        
                     StenerMaintenanceController.usernameEvent = usernameEvent;
            EventActions.addEvent(new Event(" modified Stener Set "+ SetId ,usernameEvent));

        pageChanger.swap("StenerMaintenance.fxml", event);   
   
    }
    
    private void loadData() throws IOException, SQLException {
        ObservableList table1 = FXCollections.observableArrayList();

        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all questions not on stener
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM Question WHERE QuestionID NOT IN (SELECT QuestionID FROM Answers WHERE StenerID='"+StenerId+"')");
            while(resultSet.next()){
                int id = resultSet.getInt("QuestionID");
                String content = resultSet.getString("Content");
                String help = resultSet.getString("Help");
                table1.add(new Question(id, content, help));
            }
            //get all questions on stener
            resultSet = statement.executeQuery(
                    "SELECT * FROM Question WHERE QuestionID IN (SELECT QuestionID FROM Answers WHERE StenerID='"+StenerId+"')");
            while(resultSet.next()){
                int id = resultSet.getInt("QuestionID");
                String content = resultSet.getString("Content");
                String help = resultSet.getString("Help");
                table2.add(new Question(id, content, help));
                originalQuestions.add(new Question(id, content, help));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        
        QuestionsAvailableIdColumn.setCellValueFactory(new PropertyValueFactory<>("QuestionID"));
        QuestionsAvailableTable.setItems(table1);
       
        StenerQuestionIDColumn.setCellValueFactory(new PropertyValueFactory<>("QuestionID"));
        StenerQuestionTable.setItems(table2);
        
    }

    
}
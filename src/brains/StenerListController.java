/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;


import answer.Answer;
import answer.AnswerActions;
import event.Event;
import event.EventActions;
import stener.Stener;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import stener.StenerActions;

/**
 * FXML Controller class
 *
 * @author nisarg
 */
public class StenerListController implements Initializable {

    static String usernameEvent;
    

    PageSwap pageChanger = new PageSwap();
    @FXML
    private TableView<Stener> STENERTableView;
    @FXML
    private TableColumn<Stener, Integer> STENERIDColumn;
    @FXML
    
    private TableColumn<Stener, Integer> STENERNumOfQuestionsColumn;
    @FXML
    
    private TableColumn<Stener, String> dueDateColumn;
    @FXML
    
    private TableColumn<Stener, String> EditStatusColumn;
    @FXML
    
    private TableColumn<Stener, String> resubmissionStatusColumn;
    @FXML
    
    private TableColumn<Stener, Integer> STENERDepartmentColumn;
    @FXML
    private TableColumn<Stener, Integer> setIdColumn;
    
    @FXML
    private Label deptLabel;
    
    public static Integer editID;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            loadData(editID);
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        deptLabel.setText(editID.toString());
    }    
    
    
    private void loadData(Integer editID) throws IOException, SQLException {
        ObservableList Stener = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM Stener WHERE DepNumber='" + editID + "'");
            while(resultSet.next()){
                int StenerID = resultSet.getInt("StenerId");
                int NumQuestions = resultSet.getInt("Questions");
                String dueDate = resultSet.getString("Due_date");
                String canEdit = resultSet.getString("Can_Edit");
                String resubStatus = resultSet.getString("Resubmission_Status");
                int DepNumber = resultSet.getInt("DepNumber");
                int SetID = resultSet.getInt("SetID");
                Stener.add(new Stener( StenerID,  NumQuestions, dueDate, canEdit, resubStatus, DepNumber, SetID) );
     
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //maps the question class attributes to the columns they belong to
        //make sure you fill in <> at the top with the right data things
        //include the names of the exact words after get in the get functions of Question
        STENERIDColumn.setCellValueFactory(new PropertyValueFactory<>("StenerID"));
        STENERNumOfQuestionsColumn.setCellValueFactory(new PropertyValueFactory<>("NumQuestions"));
        dueDateColumn.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
        EditStatusColumn.setCellValueFactory(new PropertyValueFactory<>("canEdit"));
        resubmissionStatusColumn.setCellValueFactory(new PropertyValueFactory<>("resubStatus"));
        STENERDepartmentColumn.setCellValueFactory(new PropertyValueFactory<>("DepNumber"));
        setIdColumn.setCellValueFactory(new PropertyValueFactory<>("SetID"));
        
        //populate table from observable list
        STENERTableView.setItems(Stener);
    }
    
    
    
    
    @FXML
    private void returnToMenu(ActionEvent event) {
                DepartmentScreenController.usernameEvent = usernameEvent;

        pageChanger.swap("DepartmentScreen.fxml", event);
    }
    
    @FXML
    private void openStener(ActionEvent event) {
        StenerFormController.stenerID = STENERTableView.getSelectionModel().getSelectedItem().getStenerID();
        StenerFormController.usernameEvent = usernameEvent;
                    

        pageChanger.swap("StenerForm.fxml", event);
    }
    
}

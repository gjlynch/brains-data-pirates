/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TablePosition;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import question.Question;
import question.QuestionActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class QuestionMaintenanceController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private Button menu;
    @FXML
    private TableView<Question> questionTable;
    @FXML
    private Button addQuestion;
    @FXML
    private Button editQuestion;
    @FXML
    private Button deleteQuestion;
    @FXML
    private TableColumn<Question, Integer> idColumn;
    @FXML
    private TableColumn<Question, String> questionColumn;
    @FXML
    private TableColumn<Question, String> helpColumn;
    @FXML
    private Label errorMessage;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        errorMessage.setText("");
        
        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    

    @FXML
    private void returnToMenu(ActionEvent event) {
        OversightScreenController.usernameEvent = usernameEvent;

        pageChanger.swap("OversightScreen.fxml", event);
    }

    @FXML
    private void addQuestion(ActionEvent event) {
        UpdateQuestionController.editID = null;
                UpdateQuestionController.usernameEvent = usernameEvent;

        pageChanger.swap("UpdateQuestion.fxml", event);
    }

    @FXML
    private void editQuestion(ActionEvent event) {
        if(questionTable.getSelectionModel().getSelectedItem()==null){
            errorMessage.setText("Please select an item");
        }
        else{
            UpdateQuestionController.usernameEvent = usernameEvent;

            UpdateQuestionController.editID = questionTable.getSelectionModel().getSelectedItem().getQuestionID();
            pageChanger.swap("UpdateQuestion.fxml", event);
        }   
    }

    @FXML
    private void deleteQuestion(ActionEvent event) throws IOException, SQLException {  
        if (questionTable.getSelectionModel().getSelectedItem() == null) {
            errorMessage.setText("Please select an item");
        } else {
            QuestionDeleteController.questionID = questionTable.getSelectionModel().getSelectedItem().getQuestionID();
            QuestionDeleteController.usernameEvent = usernameEvent;

            pageChanger.swap("QuestionDelete.fxml", event);
            errorMessage.setText("");
        }     
    }
    
    private void loadData() throws IOException, SQLException {
        ObservableList questions = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all questions from database
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM Question");
            while(resultSet.next()){
                //add question objects to ObservableList
                int id = resultSet.getInt("QuestionID");
                String content = resultSet.getString("Content");
                String help = resultSet.getString("Help");
                questions.add(new Question(id, content, help));    
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //maps the question class attributes to the columns they belong to
        //make sure you fill in <> at the top with the right data things
        //include the names of the exact words after get in the get functions of Question
        idColumn.setCellValueFactory(new PropertyValueFactory<>("QuestionID"));
        questionColumn.setCellValueFactory(new PropertyValueFactory<>("Content"));
        helpColumn.setCellValueFactory(new PropertyValueFactory<>("Help"));
        //populate table from observable list
        questionTable.setItems(questions);
        
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.cell.PropertyValueFactory;
import stener.Stener;

/**
 * FXML Controller class
 *
 * @author omega
 */
public class EventLogController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();

    @FXML
    private ComboBox<String> ComboBox;
    @FXML
    private Label EnterDateLabel;
    @FXML
    private TextArea EventsTextArea;
    @FXML
    private Label EventLogLabel;
    @FXML
    private Button ReturnToMenubtn;
    @FXML
    private Button SubmitBtn;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(EventLogController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(EventLogController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
private void loadData() throws IOException, SQLException {
        ObservableList Event = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all questions from database
            ResultSet resultSet = statement.executeQuery(
                     "SELECT DISTINCT Date FROM Event");
            while(resultSet.next()){
                //add question objects to ObservableList
                String Date= resultSet.getString("Date");
                Event.add(Date);
               
     
            }  
            ComboBox.setItems(Event);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
      
        
    }
    @FXML
    private void ComboBoxAction(ActionEvent event) {
    }

    @FXML
    private void ReturnToMenu(ActionEvent event) {
                OversightScreenController.usernameEvent = usernameEvent;

                pageChanger.swap("OversightScreen.fxml", event);

        
    }

    @FXML
    private void Submit(ActionEvent event) throws IOException, SQLException {
          String DateDisplay;
        DateDisplay = ComboBox.getSelectionModel().getSelectedItem();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet r = statement.executeQuery(
                    "SELECT * FROM Event WHERE Date ='" + DateDisplay+ "'");
            while (r.next()) {
               String username = r.getString("Username");
            int EventID = r.getInt("EventID");
           String  Date = r.getString("Date");
           String  Action = r.getString("Action");
            EventsTextArea.appendText( "\n" +Date +" "+EventID+" "+username+" "+ Action+"\n" );
            EventsTextArea.appendText( "\n"+ "\n");
            
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        
    }
    
    
}

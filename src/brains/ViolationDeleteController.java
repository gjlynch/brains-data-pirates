/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import violation.ViolationActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class ViolationDeleteController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static int vioID;
    @FXML
    private Button confirmD;
    @FXML
    private Button cancelD;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void confirmDelete(ActionEvent event) throws IOException, SQLException {       
        ViolationActions.deleteViolation(vioID);
        EventActions.addEvent(new Event(" Violation "+vioID+ "was deleted" ,usernameEvent));
        ViolationMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("ViolationMaintenance.fxml", event);
    }

    @FXML
    private void cancelDelete(ActionEvent event) {
                ViolationMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("ViolationMaintenance.fxml", event);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import answer.Answer;
import answer.AnswerActions;
import static brains.StenerListController.editID;
import stener.Stener;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import stener.StenerActions;
import question.Question;
import answer.Answer;
import event.Event;
import event.EventActions;
import java.io.File;
import java.io.FileOutputStream;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import javafx.scene.Node;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
/**
 * FXML Controller class
 *
 * @author nisarg
 */
public class StenerFormController implements Initializable {

    static String usernameEvent;

    PageSwap pageChanger = new PageSwap();
    
    ArrayList<Question> tempQuestion = new ArrayList<Question>();
    ArrayList<Answer> answers = new ArrayList<Answer>();
    
    Integer questionCounter;
    
    @FXML
    private Label questionNum;
    @FXML
    private Label questionContent;
    @FXML
    private TextField answerBox;
    @FXML
    private Button submitButton;
    @FXML
    private Label fileName;
    @FXML
    private Label helpLabel;
    
    
    
    /**
     * Initializes the controller class.
     */
    public static Integer questionID;
    public static Integer stenerID;
    @FXML
    private Button menu;
    @FXML
    private Button nextButton;
    @FXML
    private Button previousQuestion;
    @FXML
    private Button evidenceButton;
    @FXML
    private Label incompleteLabel;
    
    String canEdit;
    String resub;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        incompleteLabel.setVisible(false);
        submitButton.setVisible(false);
        
        try{
            Connection connection = CloudSQL.getConnection();
            try (Statement statement = connection.createStatement()) {
                ResultSet result = statement.executeQuery(
                        "SELECT * FROM Stener WHERE StenerID='" + stenerID + "'");
                while (result.next()) {
                    canEdit=result.getString("Can_Edit");
                    resub=result.getString("Resubmission_Status");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        try {
            loadData(stenerID);
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        if(canEdit.equals("True")){
            answerBox.setEditable(true);
        }
        else{
            answerBox.setEditable(false);
        }
        
    }
    
    private void updateScreen()
    {
        questionNum.setText(questionCounter.toString());
        questionContent.setText(tempQuestion.get(questionCounter-1).getContent());
        answerBox.setText(answers.get(questionCounter-1).getResponse());
        if (answers.get(questionCounter-1).getEvidence() != null)
        {
            fileName.setText(answers.get(questionCounter-1).getEvidence().getName());
        }
        else if (answers.get(questionCounter-1).getEvidence() == null)
        {
            fileName.setText("");
        }
        if(tempQuestion.size() > 0 && tempQuestion.size() >= questionCounter)
        {
            if (tempQuestion.get(questionCounter-1).getHelp() != null)
            {
                helpLabel.setText(tempQuestion.get(questionCounter-1).getHelp());
            }
            else {
                helpLabel.setText("");
            }
        }
        
        if(validateAnswers() && canEdit.equals("True")){
            submitButton.setVisible(true);
        }
        else{
            submitButton.setVisible(false);
        }   
        incompleteLabel.setVisible(false);
        
        
    }
    
    private void loadData(Integer stenerID) throws IOException, SQLException
    {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet result = statement.executeQuery(
                    "SELECT * FROM Question q, Answers a WHERE q.QuestionID = a.QuestionID AND a.StenerID='" + stenerID + "'");
            while(result.next()){
                tempQuestion.add(new Question(result.getInt("QuestionID"), result.getString("Content"), result.getString("Help")));
                if(canEdit.equals("False") || resub.equals("True")){
                    Blob blob = result.getBlob("Evidence");
                    if(blob != null){
                        int blobLength = (int) blob.length();
                        byte[] blobAsBytes = blob.getBytes(1, blobLength);
                        blob.free();
                        File savedFile = new File(result.getString("Evidence_Name"));
                        FileOutputStream o = new FileOutputStream(savedFile);
                        o.write(blobAsBytes);
                        o.close();
                        answers.add(new Answer(result.getInt("QuestionID"), stenerID, result.getString("Response"), savedFile));
                    }
                    else{
                        answers.add(new Answer(result.getInt("QuestionID"), stenerID));
                    }
                }
                else{
                    answers.add(new Answer(result.getInt("QuestionID"), stenerID));
                }     
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
        questionCounter = 1;
        updateScreen();
    }
    
    
    @FXML
    private void returnToStenerList(ActionEvent event) {
        pageChanger.swap("StenerList.fxml", event);
    }
    
    
    private boolean validateAnswers()
    {
        for (int i = 0; i < answers.size(); i++)
        {
            if (answers.get(i).getResponse() == null || answers.get(i).getResponse().trim().isEmpty() )
            {
                return false;
            }
        }
        return true;
    }
    
    @FXML
    private void goToNextQuestion()
    {
        answers.get(questionCounter-1).setResponse(answerBox.getText());
        if (questionCounter+1 <= tempQuestion.size())
        {
            questionCounter++;
        }
        updateScreen();
    }
    
    @FXML
    private void goToPreviousQuestion()
    {
        answers.get(questionCounter-1).setResponse(answerBox.getText());
        if (questionCounter-1 > 0)
        {
            questionCounter--;
        }
        updateScreen();
    }
    
    @FXML
    private void provideEvidence(ActionEvent event) throws IOException, SQLException
    {
        if(canEdit.equals("True")){
            answers.get(questionCounter - 1).setResponse(answerBox.getText());
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            FileChooser fileChooser = new FileChooser();
            fileChooser.setTitle("Open Resource File");
            File file = fileChooser.showOpenDialog(appStage);
            answers.get(questionCounter - 1).setEvidence(file);
            updateScreen();
        }     
    }
    
    @FXML
    private void submitStener(ActionEvent event) throws IOException, SQLException
    {
        answers.get(questionCounter-1).setResponse(answerBox.getText());
        if(validateAnswers()){
            for (Answer a : answers) {
                a.setEvidence_Name(a.getEvidence().getName());
                AnswerActions.addCompleteAnswer(a);
            }
            setEditToFalse(stenerID, editID);
            EventActions.addEvent(new Event(" Submitted Stener Form for "+ stenerID ,usernameEvent));
        StenerListController.usernameEvent = usernameEvent;

            pageChanger.swap("StenerList.fxml", event);
        }
        else{
            incompleteLabel.setVisible(true);    
        }
    }
    
    
    private void setEditToFalse(int stenerID, int editID) throws IOException, SQLException
    {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE Stener SET Can_Edit = 'False' WHERE StenerID='" + stenerID + "' AND DepNumber='" + editID + "'");
            } catch (Exception e) {
                e.printStackTrace();
            }
    }
    
}

package brains;

import stener.Stener;
import stener.StenerActions;
import answer.Answer;
import answer.AnswerActions;
import department.Department;
import department.DepartmentActions;
import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import user.*;

/**
 *
 * @author glync
 */
public class LoginController implements Initializable {
    PageSwap pageChanger = new PageSwap();
    String usernameEvent ="";
    @FXML
    private Button submitLogin;
    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label missingCredentials;
    
    @FXML
    private void handleButtonAction(ActionEvent event) throws IOException, SQLException {
        missingCredentials.setText("");
        
        if(event.getSource().equals(submitLogin)){
            if(username.getText().trim().isEmpty() || password.getText().trim().isEmpty()){
                missingCredentials.setText("Username and/or Password is missing");
            }
            else{
                User user = new User(username.getText().toLowerCase(), password.getText());
                DepartmentScreenController.depUser=user;
                if(UserActions.validate(user)){
                    usernameEvent = username.getText();
                    EventActions.addEvent(new Event(" Logged in ",usernameEvent));

                    //swap screens if logged in
                    if(UserActions.Type(user).toLowerCase().equals("oversight")){
                        OversightScreenController.usernameEvent = usernameEvent;

                        pageChanger.swap("OversightScreen.fxml", event);
                    }
                    else{
//                        StenerListController.editID = 9;
                        StenerListController.editID=UserActions.getDepNumber(user);
                        DepartmentScreenController.usernameEvent = usernameEvent;
                        pageChanger.swap("DepartmentScreen.fxml", event);
                        
                    } 
                }
                else{
                    missingCredentials.setText("Invalid Username or Password");
                }
            }
            
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}

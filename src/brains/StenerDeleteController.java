/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import static brains.StenerFormController.usernameEvent;
import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import stener.StenerActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class StenerDeleteController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static int setID;
    @FXML
    private Button cancelSD;
    @FXML
    private Button confirmSD;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void cancelStenerDelete(ActionEvent event) {
        StenerMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("StenerMaintenance.fxml", event);

    }

    @FXML
    private void confirmStenerDelete(ActionEvent event) throws IOException, SQLException {
        EventActions.addEvent(new Event(" Stener Set "+ setID +" Deleted ",usernameEvent));
        StenerMaintenanceController.usernameEvent = usernameEvent;

        StenerActions.deleteStener(setID);
        pageChanger.swap("StenerMaintenance.fxml", event);
    }
    
}


package brains;

import department.DepartmentActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import user.User;
import user.UserActions;


public class DepartmentScreenController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static User depUser;
    @FXML
    private Button stenerList;
    @FXML
    private Button violationList;
    @FXML
    private Button logoutButton;
    @FXML
    private Label depLabel;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            int depNumber = UserActions.getDepNumber(depUser);
            depLabel.setText(DepartmentActions.getDepartment(depNumber)+" Department "+depNumber);
        } catch (IOException ex) {
            Logger.getLogger(DepartmentScreenController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(DepartmentScreenController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }    

    @FXML
    private void menuItemSelected(ActionEvent event) {
        String page=null;
        
        if(event.getSource().equals(stenerList)){
        StenerListController.usernameEvent = usernameEvent;

            page="StenerList.fxml";
        }
        else if(event.getSource().equals(violationList)){
        ViolationLogController.usernameEvent = usernameEvent;

            ViolationLogController.user=depUser;
            page="ViolationLog.fxml";
        }
        else if(event.getSource().equals(logoutButton)){
            page="Login.fxml";
        }
        
        pageChanger.swap(page, event);
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import department.DepartmentActions;
import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class DepartmentDeleteController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static int depNum;
    @FXML
    private Button submitDepDelete;
    @FXML
    private Button cancelButton;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void confirmDepDelete(ActionEvent event) throws IOException, SQLException {
        
      DepartmentMaintenanceController.usernameEvent = usernameEvent;
            EventActions.addEvent(new Event(" Deleted department "+ depNum ,usernameEvent));

        DepartmentActions.deleteDepartment(depNum);
                DepartmentMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("DepartmentMaintenance.fxml", event);
    }

    @FXML
    private void cancelDepDelete(ActionEvent event) {
        
        pageChanger.swap("DepartmentMaintenance.fxml", event);
    }
    
}

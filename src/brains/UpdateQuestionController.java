/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import question.Question;
import question.QuestionActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class UpdateQuestionController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    //use Integer wrapper so we can check if it is empty/null for loading edit data
    public static Integer editID;
    @FXML
    private TextField questionID;
    @FXML
    private Label addEditLabel;
    @FXML
    private TextArea questionBox;
    @FXML
    private TextArea helpBox;
    @FXML
    private Button cancelButton;
    @FXML
    private Button submitButton;
    @FXML
    private Label statusLabel;
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        //if editID is not null
        if(editID!=null){
            //load selected question data for edit
            addEditLabel.setText("Edit Question");
            try {
                load(editID);
            } catch (IOException ex) {
                Logger.getLogger(UpdateQuestionController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(UpdateQuestionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        else{
            addEditLabel.setText("Add Question");
        }
    }

    private void load(Integer editID) throws IOException, SQLException{
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM Question WHERE QuestionID='"+editID+"'");
            while(resultSet.next()){
                questionID.setText(Integer.toString(resultSet.getInt("QuestionID")));
                questionBox.setText(resultSet.getString("Content"));
                helpBox.setText(resultSet.getString("Help"));
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void cancelQuestionUpdate(ActionEvent event) {
        editID=null;
        QuestionMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("QuestionMaintenance.fxml", event);
        
    }

    @FXML
    private void submitQuestion(ActionEvent event) throws IOException, SQLException {
        boolean added;
        if(questionID.getText().trim().isEmpty() || helpBox.getText().trim().isEmpty() 
                || questionBox.getText().trim().isEmpty()){
            statusLabel.setText("All fields are required before submitting");
        }
        else{
            if(editID != null){
                added = QuestionActions.editQuestion(new Question( Integer.parseInt(questionID.getText()), questionBox.getText(), helpBox.getText()), editID);
            }
            else{
                added = QuestionActions.addQuestion(new Question( Integer.parseInt(questionID.getText()), questionBox.getText(), helpBox.getText()));
            }          
            if(added){
                            EventActions.addEvent(new Event(" Creqted Question "+ questionID.getText() ,usernameEvent));

                
                        QuestionMaintenanceController.usernameEvent = usernameEvent;

                pageChanger.swap("QuestionMaintenance.fxml", event);
            }
            else{
                statusLabel.setText("Error updating the question database. Please try again.");
            }
        }
        
    }
    
}

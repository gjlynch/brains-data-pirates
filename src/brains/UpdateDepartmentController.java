/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import department.Department;
import department.DepartmentActions;
import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class UpdateDepartmentController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static boolean edit=false;
    static Department department;
    static int depID;
    @FXML
    private TextField nameField;
    @FXML
    private TextField locationField;
    @FXML
    private Button submitDep;
    @FXML
    private Button cancelDep;
    @FXML
    private Label actionText;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        if(edit){
            actionText.setText("Edit Department");
            loadData();
        }
        else{
            actionText.setText("Add a Department");
        }
    }

    private void loadData(){
        nameField.setText(department.getName());
        locationField.setText(department.getLocation());
        
    }

    @FXML
    private void updateDepartment(ActionEvent event) throws IOException, SQLException {
        if(edit){
            DepartmentActions.editDepartment(new Department(department.getDepNumber(), nameField.getText(), locationField.getText()));
        }
        else{
            int dID = 0;
            Connection connection = CloudSQL.getConnection();
            //get max violation id if there is one
            try (Statement statement = connection.createStatement()) {
                ResultSet r = statement.executeQuery("SELECT MAX(DepNumber) AS D FROM Department");
                while (r.next()) {
                    dID = r.getInt("D");
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            DepartmentActions.addDepartment(new Department(dID+1, nameField.getText(), locationField.getText()));
            EventActions.addEvent(new Event("Department Udated "+dID  ,usernameEvent));

        }
        DepartmentMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("DepartmentMaintenance.fxml", event);
    }

    @FXML
    private void cancelUpdate(ActionEvent event) {
                DepartmentMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("DepartmentMaintenance.fxml", event);
    }
    
}

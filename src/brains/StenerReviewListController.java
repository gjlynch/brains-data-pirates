package brains;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import stener.Stener;


public class StenerReviewListController implements Initializable {

    static String usernameEvent;

    PageSwap pageChanger = new PageSwap();
    @FXML
    private TableView<Stener> STENERTableView;
    @FXML
    private TableColumn<Stener, Integer> STENERIDColumn;
    @FXML
    
    private TableColumn<Stener, Integer> STENERNumOfQuestionsColumn;
    @FXML
    
    private TableColumn<Stener, String> dueDateColumn;
    @FXML
    
    private TableColumn<Stener, String> EditStatusColumn;
    @FXML
    
    private TableColumn<Stener, String> resubmissionStatusColumn;
    @FXML
    
    private TableColumn<Stener, Integer> STENERDepartmentColumn;
    @FXML
    private TableColumn<Stener, Integer> setIdColumn;
    @FXML
    private TableColumn<Stener, Integer> acceptedColumn;
    
    @FXML
    private Button menu;
    @FXML
    private Button selectStener;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
    
     
    private void loadData() throws IOException, SQLException {
        ObservableList Stener = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM Stener WHERE Can_Edit='False'");
            while(resultSet.next()){
                int StenerID = resultSet.getInt("StenerId");
                int NumQuestions = resultSet.getInt("Questions");
                String dueDate = resultSet.getString("Due_date");
                String canEdit = resultSet.getString("Can_Edit");
                String resubStatus = resultSet.getString("Resubmission_Status");
                int DepNumber = resultSet.getInt("DepNumber");
                int SetID = resultSet.getInt("SetID");
                String accepted = resultSet.getString("Accepted");
                Stener.add(new Stener( StenerID,  NumQuestions, dueDate, canEdit, resubStatus, DepNumber, SetID, accepted) );
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        STENERIDColumn.setCellValueFactory(new PropertyValueFactory<>("StenerID"));
        STENERNumOfQuestionsColumn.setCellValueFactory(new PropertyValueFactory<>("NumQuestions"));
        dueDateColumn.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
        EditStatusColumn.setCellValueFactory(new PropertyValueFactory<>("canEdit"));
        resubmissionStatusColumn.setCellValueFactory(new PropertyValueFactory<>("resubStatus"));
        STENERDepartmentColumn.setCellValueFactory(new PropertyValueFactory<>("DepNumber"));
        setIdColumn.setCellValueFactory(new PropertyValueFactory<>("SetID"));
        acceptedColumn.setCellValueFactory(new PropertyValueFactory<>("Accepted"));
        
        //populate table from observable list
        STENERTableView.setItems(Stener);
    }
    
    
    
    
    @FXML
    private void returnToMenu(ActionEvent event) {
                OversightScreenController.usernameEvent = usernameEvent;

        pageChanger.swap("OversightScreen.fxml", event);
    }
    
    @FXML
    private void openStener(ActionEvent event) {
        StenerReviewFormController.stenerID = STENERTableView.getSelectionModel().getSelectedItem().getStenerID();
                        StenerReviewFormController.usernameEvent = usernameEvent;

        pageChanger.swap("StenerReviewForm.fxml", event);
    }
    
}

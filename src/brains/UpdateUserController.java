/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import department.Department;
import event.Event;
import event.EventActions;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import user.User;
import user.UserActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class UpdateUserController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    static boolean edit=false;
    static User user;
    @FXML
    private TableView<Department> departmentTable;
    @FXML
    private TableColumn<Department, Integer> depNumC;
    @FXML
    private TableColumn<Department, String> depNamC;
    @FXML
    private TableColumn<Department, String> depLocC;
    @FXML
    private Label errorMessage;
    @FXML
    private Label actionLabel;
    @FXML
    private TextField usernameField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField accountTypeField;
    @FXML
    private TextField passwordField;
    @FXML
    private Button cancelUser;
    @FXML
    private Button submitUser;
    @FXML
    private Label passwordLabelNotification;
    @FXML
    private Label errorLabel;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(UpdateUserController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(UpdateUserController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadData() throws IOException,  SQLException{
        ObservableList departments = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM Department");
            while (resultSet.next()) {
                int id = resultSet.getInt("DepNumber");
                String name = resultSet.getString("Name");
                String location = resultSet.getString("Location");
                departments.add(new Department(id, name, location));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
     
        depNumC.setCellValueFactory(new PropertyValueFactory<>("DepNumber"));
        depNamC.setCellValueFactory(new PropertyValueFactory<>("Name"));
        depLocC.setCellValueFactory(new PropertyValueFactory<>("Location"));
        //populate table from observable list
        departmentTable.setItems(departments);
        if(edit){
            actionLabel.setText("Edit User");
            usernameField.setText(user.getUsername());
            nameField.setText(user.getName());
            accountTypeField.setText(user.getType());
            passwordLabelNotification.setText("Leaving blank will keep current password");
        }
        else{
            actionLabel.setText("Add a User");
            passwordLabelNotification.setText("");
        }
    }

    @FXML
    private void cancelAdd(ActionEvent event) {
                UserMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("UserMaintenance.fxml", event); 
    }

    @FXML
    private void submitAdd(ActionEvent event) throws IOException, SQLException {
        if(edit){
            if (usernameField.getText().isEmpty() || accountTypeField.getText().isEmpty() || nameField.getText().isEmpty() || departmentTable.getSelectionModel().getSelectedItem().equals(null)) {
                errorLabel.setText("All fields must be filled out before submission");
            } else {
                if(passwordField.getText().isEmpty()){
                    User u = new User(usernameField.getText(), nameField.getText(), accountTypeField.getText(), departmentTable.getSelectionModel().getSelectedItem().getDepNumber());
                    UserActions.editUserNoPass(u, user.getUsername());
                                    UserMaintenanceController.usernameEvent = usernameEvent;
            EventActions.addEvent(new Event(" Edited User, password unchanged "+ usernameField.getText() ,usernameEvent));

                    pageChanger.swap("UserMaintenance.fxml", event);
                }
                else{
                    User u = new User(usernameField.getText(), passwordField.getText(), nameField.getText(), accountTypeField.getText(), departmentTable.getSelectionModel().getSelectedItem().getDepNumber());
                    UserActions.editUser(u, user.getUsername());
                                    UserMaintenanceController.usernameEvent = usernameEvent;
            EventActions.addEvent(new Event(" Editied User ",usernameEvent));

                    pageChanger.swap("UserMaintenance.fxml", event);  
                }
            }
        }
        else{
            if(usernameField.getText().isEmpty() || passwordField.getText().isEmpty() || accountTypeField.getText().isEmpty() || nameField.getText().isEmpty() || departmentTable.getSelectionModel().getSelectedItem().getName().equals(null) ){
                errorLabel.setText("All fields must be filled out before submission");
            }
            else{
                User u = new User(usernameField.getText(), passwordField.getText(), nameField.getText(), accountTypeField.getText(), departmentTable.getSelectionModel().getSelectedItem().getDepNumber());
                UserActions.addUser(u);
                                UserMaintenanceController.usernameEvent = usernameEvent;
            EventActions.addEvent(new Event(" Added new user "+ usernameField.getText() ,usernameEvent));

                pageChanger.swap("UserMaintenance.fxml", event); 
            }    
        }
    }

    
}

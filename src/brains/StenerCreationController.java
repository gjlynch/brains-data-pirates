/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import stener.Stener;
import stener.StenerActions;
import answer.AnswerActions;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.beans.InvalidationListener;
import javafx.beans.Observable;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.table.DefaultTableModel;
import question.Question;
import answer.Answer;
import answer.AnswerActions;
import event.Event;
import event.EventActions;
/**
 * FXML Controller class
 *
 * @author omega
 */
public class StenerCreationController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private TableView<Question> QuestionsAvailableTable;
    @FXML
    private TableColumn<Question, Integer> QuestionsAvailableIdColumn;
    @FXML
    private TableView<Question> StenerQuestionTable;
    @FXML
    private TableColumn<Question, Integer> StenerQuestionIDColumn;
    @FXML
    private Label QuestionsAvailableLabel;
    @FXML
    private TextField IdTextField;
    @FXML
    private TextField dueDateTextfield;
    @FXML
    private Label STENERIDLabel;
    @FXML
    private Label DueDateLabel;
    @FXML
    private TextField SetIdTextField;
    @FXML
    private Label SetIDLabel;
    @FXML
    private TextArea QuestionContentTextArea;
    @FXML
    private Button CancelBtn;
    @FXML
    private Button AddQuest;
    @FXML
    private Button RemoveQuest;
    @FXML
   
    private Label SubmitErrormessage;
    @FXML
    private Label StenerIdErrorLabel;
    @FXML
    private Label DueDateErrorMessageLabel;
    @FXML
    private Label SetIDErrorMessageLabel;
    
    //question to be added to stener
    ObservableList questions = FXCollections.observableArrayList();
 

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
       try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
       
       QuestionsAvailableTable.getSelectionModel().selectedItemProperty().addListener(new InvalidationListener() {
           @Override
           public void invalidated(Observable newValue) {
               QuestionContentTextArea.clear(); QuestionContentTextArea.appendText(QuestionsAvailableTable.getSelectionModel().getSelectedItem().getContent());
           }
       });
    }

        private void loadData() throws IOException, SQLException {
            ObservableList questions = FXCollections.observableArrayList();
            Connection connection = CloudSQL.getConnection();
            try (Statement statement = connection.createStatement()) {
                //get all questions from database
                ResultSet resultSet = statement.executeQuery(
                         "SELECT * FROM Question");
                while(resultSet.next()){
                    //add question objects to ObservableList
                    int id = resultSet.getInt("QuestionID");
                     String content = resultSet.getString("Content");
                    String help = resultSet.getString("Help");
                     questions.add(new Question(id, content, help));


                }  
            } catch (Exception e) {
                e.printStackTrace();
        }
        
        //maps the question class attributes to the columns they belong to
        //make sure you fill in <> at the top with the right data things
        //include the names of the exact words after get in the get functions of Question
        QuestionsAvailableIdColumn.setCellValueFactory(new PropertyValueFactory<>("QuestionID"));
        
        //populate table from observable list
       QuestionsAvailableTable.setItems(questions);
        
    }

    @FXML
    private void CancelAction(ActionEvent event) {
                            StenerMaintenanceController.usernameEvent = usernameEvent;

        pageChanger.swap("StenerMaintenance.fxml", event);
    
    }

    @FXML
    private void AddQuestion(ActionEvent event) {
        ObservableList table2 = StenerQuestionTable.getItems();
        table2.add(QuestionsAvailableTable.getSelectionModel().getSelectedItem());
        QuestionsAvailableTable.getItems().removeAll( QuestionsAvailableTable.getSelectionModel().getSelectedItem());
        StenerQuestionIDColumn.setCellValueFactory(new PropertyValueFactory<>("QuestionID"));
        StenerQuestionTable.setItems(table2);     
    }

    @FXML
    private void RemoveQuestion(ActionEvent event) {
        ObservableList table2 = QuestionsAvailableTable.getItems();
        table2.add(StenerQuestionTable.getSelectionModel().getSelectedItem());
        StenerQuestionTable.getItems().removeAll( StenerQuestionTable.getSelectionModel().getSelectedItem());
        QuestionsAvailableTable.setItems(table2);
    }

    @FXML
    private void Submit(ActionEvent event) throws IOException, SQLException {
        boolean added =false ;   
        if (IdTextField.getText().isEmpty()||SetIdTextField.getText().isEmpty() ||dueDateTextfield.getText().isEmpty()) {
            
            SubmitErrormessage.setText("StenerId, DueDate and SetId cannot be empty");
            SubmitErrormessage.setVisible(true);
        }
        else {          
            int Id;
            Id = Integer.parseInt(IdTextField.getText());

            String duedate = IdTextField.getText();
            int SetId = Integer.parseInt(SetIdTextField.getText());
            String canEdit ="True";
            String ResubStatus ="False";
            int DepNumber = 0;
            int numQuestion = StenerQuestionTable.getItems().size();   

            StenerActions.addTemplateStener(new Stener( Id,numQuestion,duedate,canEdit,ResubStatus,SetId));
            for (Question q : StenerQuestionTable.getItems()) {
                int questionID = q.getQuestionID();
                added =AnswerActions.addAnswer(new Answer(questionID,Id));
            }
            if(added){
            EventActions.addEvent(new Event(" Stener Created " +Id ,usernameEvent));

             StenerMaintenanceController.usernameEvent = usernameEvent;

                pageChanger.swap("StenerMaintenance.fxml", event);
            }   

        }
     
    }
   
}
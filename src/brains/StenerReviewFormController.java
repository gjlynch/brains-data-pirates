/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import answer.Answer;
import event.Event;
import event.EventActions;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.FileNameMap;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.xml.bind.DatatypeConverter;
import question.Question;
import stener.StenerActions;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class StenerReviewFormController implements Initializable {

    static String usernameEvent;


    PageSwap pageChanger = new PageSwap();
    
    ArrayList<Question> tempQuestion = new ArrayList<Question>();
    ArrayList<Answer> tempAnswer = new ArrayList<Answer>();
    ArrayList<File> tempFile = new ArrayList<File>();
    ArrayList<Answer> answers = new ArrayList<Answer>();
    
    Integer questionCounter;
    
    @FXML
    private Label questionNum;
    @FXML
    private Label questionContent;
    @FXML
    private TextField answerBox;
    private Button submitButton;
    @FXML
    private Label fileName;
    
    /**
     * Initializes the controller class.
     */
    public static Integer questionID;
    public static Integer stenerID;
    @FXML
    private Button menu;
    @FXML
    private Button nextButton;
    @FXML
    private Button previousQuestion;
    @FXML
    private Button evidenceButton;
    @FXML
    private Button Reject;
    @FXML
    private Button Accept;
    @FXML
    private Button addViolation;
    @FXML
    private Label violationNotification;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        Accept.setVisible(false);
        Reject.setVisible(false);
        try {
            loadData(stenerID);
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
         
    }
    
    private void updateScreen()
    {
        questionNum.setText(questionCounter.toString());
        questionContent.setText(tempQuestion.get(questionCounter-1).getContent());
        answerBox.setText(answers.get(questionCounter-1).getResponse());
        if (answers.get(questionCounter-1).getEvidence() != null)
        {
            fileName.setText(answers.get(questionCounter-1).getEvidence().getName());
        }
        else if (answers.get(questionCounter-1).getEvidence() == null)
        {
            fileName.setText("");
        }
       
    }
    
    private void loadData(Integer stenerID) throws IOException, SQLException
    {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all questions from database
            ResultSet result = statement.executeQuery(
                    "SELECT * FROM Question q, Answers a WHERE q.QuestionID = a.QuestionID AND a.StenerID='" + stenerID + "'");
            while(result.next()){
                tempQuestion.add(new Question(result.getInt("QuestionID"), result.getString("Content"), result.getString("Help")));
                answers.add(new Answer(result.getInt("QuestionID"), stenerID, result.getString("Response")));
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
        questionCounter = 1;
        if (questionCounter + 1 > tempQuestion.size()) {
            if (!hasViolation()) {
                Accept.setVisible(true);
            }
            Reject.setVisible(true);
        }
        if(hasViolation()){
            violationNotification.setText("This Stener has a violation");
        }
        updateScreen();
    }
    
    
    @FXML
    private void returnToStenerList(ActionEvent event) {
                StenerReviewListController.usernameEvent = usernameEvent;

        pageChanger.swap("StenerReviewList.fxml", event);
    }
    
    
    private boolean validateAnswers()
    {
        for (int i = 0; i < answers.size(); i++)
        {
            if (answers.get(i).getResponse() == null || answers.get(i).getResponse().trim().isEmpty() )
            {
                return false;
            }
        }
        return true;
    }
    
    @FXML
    private void goToNextQuestion() throws IOException, SQLException {
        answers.get(questionCounter - 1).setResponse(answerBox.getText());
        if (questionCounter + 1 <= tempQuestion.size()) {
            questionCounter++;
        }
        if (questionCounter + 1 > tempQuestion.size()) {
            if(!hasViolation()){
                Accept.setVisible(true);
            }
            Reject.setVisible(true);
        }
        updateScreen();
    }

    @FXML
    private void goToPreviousQuestion() {
        answers.get(questionCounter - 1).setResponse(answerBox.getText());
        Accept.setVisible(false);
        Reject.setVisible(false);
        if (questionCounter - 1 > 0) {
            questionCounter--;
        }
        updateScreen();
    }
    
    @FXML
    private void provideEvidence(ActionEvent event) throws IOException, SQLException
    {
        String evidence=null;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all questions from database
            ResultSet result = statement.executeQuery(
                    "SELECT Evidence_Name AS e FROM Answers Where StenerID='"+stenerID+"' AND QuestionID='"+questionID+"'");
            while (result.next()) {
                evidence=result.getString("e");
                evidence=evidence.substring(evidence.indexOf("."), evidence.length());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        try (Statement statement = connection.createStatement()) {
            //get all questions from database
            ResultSet result = statement.executeQuery(
                    "SELECT Evidence AS e FROM Answers Where StenerID='"+stenerID+"' AND QuestionID='"+questionID+"'");
            while(result.next()){
                Blob blob = result.getBlob("e");
                
                int blobLength = (int) blob.length();
                byte[] blobAsBytes = blob.getBytes(1, blobLength);
                blob.free();
                String s = new String(blobAsBytes, StandardCharsets.UTF_8);
              
                
                Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
                FileChooser fileChooser = new FileChooser();
                fileChooser.setTitle("Save file");
                fileChooser.setInitialFileName(stenerID+"_"+questionCounter+"_evidence");
                fileChooser.getExtensionFilters().add(fileExtension(evidence));
                File savedFile = fileChooser.showSaveDialog(appStage);
                
                FileOutputStream o = new FileOutputStream(savedFile);
                o.write(blobAsBytes);
                o.close();

                
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    @FXML
    private void acceptOrReject(ActionEvent event) throws IOException, SQLException
    {
        String completionStatus=null;
        if(event.getSource().equals(Accept)){
            completionStatus="True";
        }
        else if(event.getSource().equals(Reject)){
            completionStatus="False";
            setEditToTrue();
        }
        
        Connection connection = CloudSQL.getConnection(); 
        
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE Stener SET Accepted='"+completionStatus+"' WHERE "+
                            "StenerID='"+stenerID+"'");
           EventActions.addEvent(new Event("Stener "+ stenerID + "Completion status changed to "+completionStatus+ " in review" ,usernameEvent));

        } catch (Exception e) {
            e.printStackTrace();
        }
                StenerReviewListController.usernameEvent = usernameEvent;

        pageChanger.swap("StenerReviewList.fxml", event);
    }
    
    private FileChooser.ExtensionFilter fileExtension(String type){
        FileChooser fileChooser = new FileChooser();
        FileChooser.ExtensionFilter extFilter=null;
        switch(type){
            case ".txt":
                extFilter = new FileChooser.ExtensionFilter("TEXT File", "*.txt");
                break;
            case ".docx":
                extFilter = new FileChooser.ExtensionFilter("Word Document", "*.docx");
                break;
            case ".doc":
                extFilter = new FileChooser.ExtensionFilter("Word Document", "*.doc");
                break;
            case ".pdf":
                extFilter = new FileChooser.ExtensionFilter("PDF Document", "*.pdf");
                break;
            case ".png":
                extFilter = new FileChooser.ExtensionFilter("PNG Image", "*.png");
                break;
            case ".jpg":
                extFilter = new FileChooser.ExtensionFilter("JPG Image", "*.jpg");
                break;
            case ".jpeg":
                extFilter = new FileChooser.ExtensionFilter("JPEG Image", "*.jpeg");
                break;
        }
        return extFilter;
    }

    private void setEditToTrue() throws IOException, SQLException
    {
        int dep = 0;
        
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet r = statement.executeQuery("SELECT DepNumber FROM Stener WHERE StenerID='" +stenerID+"'");
            while(r.next()){
                dep=r.getInt("DepNumber");
                System.out.println(dep);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE Stener SET Can_Edit='True', Resubmission_Status='True' WHERE StenerID='" + stenerID + "' AND DepNumber='"+dep+ "'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @FXML
    private void createViolation(ActionEvent event) throws IOException, SQLException {
        UpdateViolationController.depNumber= StenerActions.getDepartmentNumber(stenerID);
        UpdateViolationController.StenerID= stenerID;
        UpdateViolationController.QuestionID = tempQuestion.get(questionCounter-1).getQuestionID();
                UpdateViolationController.usernameEvent = usernameEvent;

        pageChanger.swap("UpdateViolation.fxml", event);
    }
    
    private boolean hasViolation() throws IOException, SQLException{
        boolean vio = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet r = statement.executeQuery("SELECT * FROM Violation WHERE StenerID='"+stenerID+"'");
            while(r.next()){
                String s=r.getString("Origin");
                if(s.equals(null)){
                    vio=false;
                }
                else{
                    vio=true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return vio;
    }
    
}

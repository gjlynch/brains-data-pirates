package brains;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.SQLNonTransientConnectionException;


public class CloudSQL {
    public static Connection getConnection() throws SQLNonTransientConnectionException 
    ,IOException, SQLException {
        String instanceConnectionName = "brains-261121:us-east4:brain";//<Your instanceConnectionName>
        String databaseName = "Brains";//<Database name from which u want to list tables>


        String IP_of_instance = "35.245.170.225";//<IP address of the instance>
        String username = "gjlynch";//<mysql username>
        String password = "";//<mysql password>
        

        String jdbcUrl = String.format(
                "jdbc:mysql://%s/%s?cloudSqlInstance=%s",
                IP_of_instance,
                databaseName,
                instanceConnectionName);
        
        Connection connection = DriverManager.getConnection(jdbcUrl, username, password);
        
        return connection;
    }
}

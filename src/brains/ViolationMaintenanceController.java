/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import question.Question;
import violation.Violation;

/**
 * FXML Controller class
 *
 * @author glync
 */
public class ViolationMaintenanceController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private Button menu;
    @FXML
    private TableView<Violation> violationTable;
    @FXML
    private Button editQuestion;
    @FXML
    private Button deleteQuestion;
    @FXML
    private Label errorMessage;
    @FXML
    private TableColumn<Violation, Integer> vioIDC;
    @FXML
    private TableColumn<Violation, Integer> questionIDC;
    @FXML
    private TableColumn<Violation, Integer> stenerIDC;
    @FXML
    private TableColumn<Violation, String> depNumC;
    @FXML
    private TableColumn<Violation, String> timeC;
    @FXML
    private TableColumn<Violation, String> dateC;
    @FXML
    private TableColumn<Violation, String> rateC;
    @FXML
    private TableColumn<Violation, String> statusC;
    @FXML
    private TableColumn<Violation, String> descriptionC;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        try {
            // TODO
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(ViolationMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(ViolationMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void loadData() throws IOException, SQLException{
        ObservableList violations = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all violations from database
            ResultSet resultSet = statement.executeQuery(
                    "SELECT * FROM Violation");
            while (resultSet.next()) {
                //add violation objects to ObservableList
                String time = resultSet.getString("Time");
                String date = resultSet.getString("Date");
                String description = resultSet.getString("Description");
                String rating = resultSet.getString("Rating");
                int department = Integer.parseInt(resultSet.getString("Origin"));
                int questionID = resultSet.getInt("QuestionID");
                int stenerID = resultSet.getInt("StenerID");
                int violationID = resultSet.getInt("ViolationID");
                String status = resultSet.getString("Status");
                violations.add(new Violation(time, date, description, rating, department, questionID, stenerID, violationID, status));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        //maps the question class attributes to the columns they belong to
        //make sure you fill in <> at the top with the right data things
        //include the names of the exact words after get in the get functions of Question
        vioIDC.setCellValueFactory(new PropertyValueFactory<>("ViolationID"));
        questionIDC.setCellValueFactory(new PropertyValueFactory<>("QuestionID"));
        stenerIDC.setCellValueFactory(new PropertyValueFactory<>("StenerID"));
        depNumC.setCellValueFactory(new PropertyValueFactory<>("Department"));
        timeC.setCellValueFactory(new PropertyValueFactory<>("Time"));
        dateC.setCellValueFactory(new PropertyValueFactory<>("Date"));
        rateC.setCellValueFactory(new PropertyValueFactory<>("Rating"));
        statusC.setCellValueFactory(new PropertyValueFactory<>("Status"));
        descriptionC.setCellValueFactory(new PropertyValueFactory<>("Description"));
        //populate table from observable list
        violationTable.setItems(violations);
    }

    @FXML
    private void returnToMenu(ActionEvent event) {
                OversightScreenController.usernameEvent = usernameEvent;

        pageChanger.swap("OversightScreen.fxml", event);
    }


    @FXML
    private void editViolation(ActionEvent event) {
        UpdateViolationController.v=violationTable.getSelectionModel().getSelectedItem();
        UpdateViolationController.editViolation=true;
                        UpdateViolationController.usernameEvent = usernameEvent;

        pageChanger.swap("UpdateViolation.fxml", event);
    }

    @FXML
    private void deleteViolation(ActionEvent event) {
        ViolationDeleteController.vioID=violationTable.getSelectionModel().getSelectedItem().getViolationID();
                       ViolationDeleteController.usernameEvent = usernameEvent;

        pageChanger.swap("ViolationDelete.fxml", event);
    }
    
}

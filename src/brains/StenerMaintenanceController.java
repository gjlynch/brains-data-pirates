/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import answer.Answer;
import answer.AnswerActions;
import event.Event;
import event.EventActions;
import stener.Stener;
import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import stener.StenerActions;


/**
 * FXML Controller class
 *
 * @author omega
 */
public class StenerMaintenanceController implements Initializable {

    static String usernameEvent;
    PageSwap pageChanger = new PageSwap();
    @FXML
    private TableView<Stener> STENERTableView;
    @FXML
    private TableColumn<Stener, Integer> STENERIDColumn;
    @FXML
    
    private TableColumn<Stener, Integer> STENERNumOfQuestionsColumn;
    @FXML
    
    private TableColumn<Stener, String> dueDateColumn;
    @FXML
    
    private TableColumn<Stener, String> EditStatusColumn;
    @FXML
    
    private TableColumn<Stener, String> resubmissionStatusColumn;
    @FXML
    
    private TableColumn<Stener, Integer> STENERDepartmentColumn;
    @FXML
    private TableColumn<Stener, Integer> setIdColumn;
    
    @FXML
    private Button DeleteBtn;
    @FXML
    private Button ModifyBtn;
    @FXML
    private Button AssignBtn;
    @FXML
    private Button CreateBtn;
    @FXML
    private Button ReturnBtn;
    @FXML
    private Label errorMessage;
    @FXML
    private Label assignedLabel;
  

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        errorMessage.setText("");
        assignedLabel.setText("");
        
        try {
            loadData();
        } catch (IOException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(QuestionMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }    
      private void loadData() throws IOException, SQLException {
        ObservableList Stener = FXCollections.observableArrayList();
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            //get all questions from database
            ResultSet resultSet = statement.executeQuery(
                     "SELECT * FROM Stener WHERE DepNumber is NULL");
            while(resultSet.next()){
                //add question objects to ObservableList
                int StenerID = resultSet.getInt("StenerId");
                int NumQuestions = resultSet.getInt("Questions");
                String dueDate = resultSet.getString("Due_date");
                String canEdit = resultSet.getString("Can_Edit");
                String resubStatus = resultSet.getString("Resubmission_Status");
                int DepNumber = resultSet.getInt("DepNumber");
                int SetID = resultSet.getInt("SetID");
                Stener.add(new Stener( StenerID,  NumQuestions, dueDate, canEdit, resubStatus, DepNumber, SetID) );
     
            }  
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //maps the question class attributes to the columns they belong to
        //make sure you fill in <> at the top with the right data things
        //include the names of the exact words after get in the get functions of Question
        STENERIDColumn.setCellValueFactory(new PropertyValueFactory<>("StenerID"));
        STENERNumOfQuestionsColumn.setCellValueFactory(new PropertyValueFactory<>("NumQuestions"));
        dueDateColumn.setCellValueFactory(new PropertyValueFactory<>("dueDate"));
        EditStatusColumn.setCellValueFactory(new PropertyValueFactory<>("canEdit"));
        resubmissionStatusColumn.setCellValueFactory(new PropertyValueFactory<>("resubStatus"));
        STENERDepartmentColumn.setCellValueFactory(new PropertyValueFactory<>("DepNumber"));
        setIdColumn.setCellValueFactory(new PropertyValueFactory<>("SetID"));
 
        //populate table from observable list
        STENERTableView.setItems(Stener);
        
    }

    @FXML
    private void returnToMenu(ActionEvent event) {
        OversightScreenController.usernameEvent = usernameEvent;

         pageChanger.swap("OversightScreen.fxml", event);
    }

    @FXML
    private void Delete(ActionEvent event) {
        if(STENERTableView.getSelectionModel().getSelectedItem()==null){
            errorMessage.setText("error please select an item");
        }
        else{
                    StenerDeleteController.usernameEvent = usernameEvent;

            StenerDeleteController.setID =STENERTableView.getSelectionModel().getSelectedItem().getSetID();
            pageChanger.swap("StenerDelete.fxml", event);
            errorMessage.setText("");       
        }
    }

    @FXML
    private void ModifyStener(ActionEvent event) {
        if (STENERTableView.getSelectionModel().getSelectedItem() == null) {
            errorMessage.setText(" error please select an item");
        }
        else    
        {
            StenerModificationController.usernameEvent = usernameEvent;

            StenerModificationController.StenerId = STENERTableView.getSelectionModel().getSelectedItem().getStenerID();
            StenerModificationController.DueDate = STENERTableView.getSelectionModel().getSelectedItem().getDueDate();
            StenerModificationController.SetId = STENERTableView.getSelectionModel().getSelectedItem().getSetID();
            pageChanger.swap("StenerModification.fxml", event);
        }
        assignedLabel.setText("");
    }

    @FXML
    private void Assign(ActionEvent event) {
        ArrayList dep = new ArrayList();
        Stener selectedRow = STENERTableView.getSelectionModel().getSelectedItem();
        //get all department numbers and add to arraylist
        try (Connection connection = CloudSQL.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet r= statement.executeQuery("SELECT DepNumber FROM Department");
            while(r.next()){
                dep.add(r.getString("DepNumber"));
            }     
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        //get current max id
        int max=0;
        try (Connection connection = CloudSQL.getConnection(); Statement statement = connection.createStatement()) {
            ResultSet r= statement.executeQuery("SELECT MAX(StenerID) AS ID FROM Stener");
            while(r.next()){
                max= r.getInt("ID");
            }     
        } catch (Exception e) {
            e.printStackTrace();
        }
        //add department assigned steners with incremented stenerID
        for(int i=0; i < dep.size();i++){
            try {
                StenerActions.assignStener(new Stener(max+(i+1), selectedRow.getNumQuestions(), selectedRow.getDueDate(),
                        selectedRow.getCanEdit(), selectedRow.getResubStatus(), Integer.parseInt((String)dep.get(i)), selectedRow.getSetID()));
                
                try (Connection connection = CloudSQL.getConnection(); Statement statement = connection.createStatement()) {
                    //Added Event
                     EventActions.addEvent(new Event(" Assigned Stener " + selectedRow.getStenerID() ,usernameEvent));

                    ResultSet r = statement.executeQuery("SELECT QuestionID FROM Answers WHERE StenerID='"+selectedRow.getStenerID()+"'");
                    while (r.next()) {
                        AnswerActions.addAnswer(new Answer(r.getInt("QuestionID"),max+(i+1)));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (IOException ex) {
                Logger.getLogger(StenerMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                Logger.getLogger(StenerMaintenanceController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        assignedLabel.setText("Stener assigned");
        
    }

    @FXML
    private void CreateStener(ActionEvent event) {
                            StenerCreationController.usernameEvent = usernameEvent;

        pageChanger.swap("StenerCreation.fxml", event);

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package brains;

import java.io.IOException;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;


public class PageSwap {
    
    public PageSwap(){ }
    
    public void swap(String page, ActionEvent event) {
        try{
            Parent newPage = FXMLLoader.load(getClass().getResource(page));
            Scene scene = new Scene(newPage);
            Stage appStage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            appStage.setScene(scene);
            appStage.show();   
        }
        catch (IOException e) {
            System.out.println("Unable to change screens");
            e.printStackTrace();
            
        }
        
    }
    
}

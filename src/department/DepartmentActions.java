/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package department;

import brains.CloudSQL;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import question.Question;

/**
 *
 * @author glync
 */
public class DepartmentActions { 
    public static boolean addDepartment(Department department) throws IOException, SQLException{
        boolean added = false;
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                     "INSERT INTO Department (DepNumber, Name, Location)" +
                        "VALUES ('"+department.getDepNumber()+"', '"+department.getName()
                             +"', '"+department.getLocation()+"')");
            added=true;
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return added;
    }
    
    public static String getDepartment(int id) throws IOException, SQLException{
        String dep = "";
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            ResultSet r = statement.executeQuery(
                     "SELECT * FROM Department WHERE DepNumber='"+id+"'");
            while(r.next()){
               dep=r.getString("Name"); 
            }       
        } catch (Exception e) {
            e.printStackTrace();
        }
   
        return dep;
    }
    
    public static void editDepartment(Department department) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {
            statement.executeUpdate(
                    "UPDATE Department SET Name='"+department.getName()+"', Location='"+department.getLocation()
                            +"' WHERE DepNumber='"+department.getDepNumber()+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
    public static void deleteDepartment(int department) throws IOException, SQLException {
        Connection connection = CloudSQL.getConnection();
        try (Statement statement = connection.createStatement()) {  
            ResultSet r= statement.executeQuery("SELECT * FROM Stener WHERE DepNumber='"+department+"'");
            while(r.next()){
                statement.executeUpdate("DELETE FROM Answers WHERE StenerID='"+r.getInt("StenerID")+"'");
            }
            statement.executeUpdate("DELETE FROM Violation WHERE Origin='"+department+"'");
            statement.executeUpdate("DELETE FROM Stener WHERE DepNumber='"+department+"'");
            statement.executeUpdate("UPDATE User SET Department='NULL' WHERE Department='"+department+"'");
            statement.executeUpdate("DELETE FROM Department WHERE DepNumber='"+department+"'");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
}
